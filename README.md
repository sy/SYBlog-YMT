# SYBlog-YMT

YMT是一款SYBlog的MD风格主题（不完全为100%MD）

界面参考：https://blog.sylingd.com

# 使用方法

将`assets`文件夹中的文件自行上传（这里面是CSS/JS文件）

将`views`文件夹中的文件替换SYBlog`application/views`中的同名文件，并修改CSS/JS地址即可

# 可变主题

使用可变主题：将color.php上传到任意支持php的空间，并修改header.php中的地址

更换主题：使用`changeColor`可更换主题，这是按钮事件demo：

```
$("#changecolor").bind("click",
function() {
	$("body").removeClass("body-" + window.color);
	window.color = changeColor(window.color);
	$("body").addClass("body-" + window.color)
});
```